***Settings***
Documentation       Representação da página Equipos com suas ações e elementos

***Variables***
${EQUIPOS_FORM}       css:a[href="/equipos/register"]


****Keywords***
Register New Equipo
    [Arguments]     ${name}     ${daily_price}
    Input Text      id:equipo-name     ${name}
    Input Text      id:daily_price     ${daily_price}

    Click Element   xpath://button[text()="CADASTRAR"]  

***Keywords***
Go To Equipos
    Wait Until Element Is Visible  ${NAV_EQUIPOS}     5
    Click Element                  ${NAV_EQUIPOS}