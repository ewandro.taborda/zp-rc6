****Settings***
Documentation       Keywords do Projeto

***Keywords***
##LOGIN##
Dado que acesso a página Login
    Go To   ${base_url}
    
Quando submeto minhas credenciais
    [Arguments]     ${email}       ${password}
    
    Login With      ${email}       ${password}
      
Então devo ser autenticado
    #Wait Until Element Is Visible   xpath://strong[text()='Sair']       5
    Wait Until Page Contains  Aluguéis      5
    
Então devo ver uma mensagem de alerta
    [Arguments]     ${expect_message}

    Wait Until Element Contains     ${TOASTER_ERROR_P}   ${expect_message}        

##SAVE CUSTOMERS##
Dado que acesso o formulário de cadastro de clientes
    Go To Customers
    Wait Until Element Is Visible  ${CUSTOMERS_FORM}    5
    Click Element                  ${CUSTOMERS_FORM}

E que eu tenho o seguinte cliente:
    [Arguments]     ${name}     ${cpf}      ${address}      ${phone_number}

    Remove Customer By Cpf  ${cpf}
    
    Set Test Variable   ${name}  
    Set Test Variable   ${cpf}
    Set Test Variable   ${address}
    Set Test Variable   ${phone_number}

Quando faço a inclusão desse cliente
    Register New Customer   ${name}     ${cpf}      ${address}      ${phone_number}

E esse cliente deve aparecer na listagem
    ${cpf_formatado}=       Format Cpf      ${cpf} 
    Go Back  
    Wait Until Element Is Visible   ${CUSTOMER_LIST}       5  
    Table Should Contain            ${CUSTOMER_LIST}       ${cpf_formatado}  
    
Mas esse cliente já existe
     Insert Customer     ${name}     ${cpf}      ${address}      ${phone_number}

Então devo ver a notificação:
    [Arguments]     ${expect_notice}
    
    Wait Until Element Contains  ${TOASTER_SUCCESS}       ${expect_notice}        5

Então devo ver a mensagem de validação do campo:   
    [Arguments]     ${msg_validation}
    Wait Until Page Contains        ${msg_validation}  

Então devo ver uma notificação com o erro
    [Arguments]     ${expect_message}

    Wait Until Element Contains     ${TOASTER_ERROR_S}   ${expect_message} 

Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatório
    Wait Until Element Contains     ${LABEL_NAME}            Nome é obrigatório          5
    Wait Until Element Contains     ${LABEL_CPF}             CPF é obrigatório           5
    Wait Until Element Contains     ${LABEL_ADDRESS}         Endereço é obrigatório      5
    Wait Until Element Contains     ${LABEL_PHONE}           Telefone é obrigatório      5

##EQUIPOS##
Dado que acesso o formulário de cadastro de equipamentos
    Go To Equipos
    Wait Until Element Is Visible  ${EQUIPOS_FORM}    5
    Click Element                  ${EQUIPOS_FORM}

E que eu tenho o seguinte equipamento:
    [Arguments]     ${name}     ${daily}

    Remove Equipo By Name   ${name}    
    
    Set Test Variable   ${name}  
    Set Test Variable   ${daily}

Quando faço a inclusão desse equipamento
    Register New Equipo     ${name}     ${daily}    

Mas esse equipamento já está cadastrado
    Insert Equipo     ${name}     ${daily}

##REMOVE CUSTOMERS##
Dado que eu tenho um cliente indesejado:
    [Arguments]      ${name}     ${cpf}      ${address}      ${phone_number}
    
    Remove Customer By Cpf       ${cpf}      
    Insert Customer              ${name}     ${cpf}      ${address}      ${phone_number}

    Set Test Variable            ${cpf}  

E acesso a listagem de clientes
    Go To Customers

Quando eu removo esse cliente

    ${cpf_formatado}=   Format Cpf      ${cpf}

    Set Test Variable  ${cpf_formatado}

    Go To Customer Details      ${cpf_formatado}
    Click Remove Customer

E esse cliente não deve aparecer na listagem
    Wait Until Page Does Not Contain    ${cpf_formatado}  

## Contrato de Locação
Dado que eu tenho o seguinte cliente cadastrado
    [Arguments]     ${file_name}

    ${customer}=     Get Json File      customers/${file_name}
    
    Delete Customer     ${customer['cpf']}
    Post Customer       ${customer}

    Set Test Variable   ${customer}

E este cliente deseja alugar o seguinte equipamento
    [Arguments]     ${file_name}

    ${equipo}=             Get Json File      equipos/${file_name}
    # Remove Equipo By Name   ${equipo['name']} 
    Post Equipo             ${equipo}

    Set Test Variable       ${equipo}

E acesso o formulário de cadastro de contratos
    Go To Contracts
    Wait Until Element Is Visible  ${CONTRACTS_FORM}    5
    Click Element                  ${CONTRACTS_FORM}

Quando faço um novo contrato de locação
    Create New Contract  ${customer['name']}  ${equipo['name']}
   

 