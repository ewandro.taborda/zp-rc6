****Settings***
Documentation         Camada de serviços do projeto de automação

Library               Collections
Library               RequestsLibrary

Resource              ../resources/helpers.robot


***Variables***
${base_api_url}       http://zepalheta-api:3333

***Keywords***

##HELPERS##
Get Session Token
    ${resp}=    Post Session        admin@zepalheta.com.br    pwd123
    ${token}=   Convert To String   Bearer ${resp.json()['token']}

    [Return]    ${token}


##POST / SESSIONS##
Post Session
    [Arguments]       ${email}               ${password}
    
    Create Session    zp-api                 ${base_api_url}
    
    &{headers}=       Create Dictionary      content-type=application/json
    &{payload}=       Create Dictionary      email=${email}   password=${password}

    ${resp}=          Post Request           zp-api      /sessions          data=${payload}     headers=${headers}
    
    [Return]          ${resp}  


##POST / CUSTOMERS##
Post Customer
    [Arguments]         ${payload}
    
    Create Session      zp-api              ${base_api_url}

    ${token}=           Get Session Token
    ${headers}          Create Dictionary   content-type=application/json   authorization=${token}
    ${resp}=            Post Request        zp-api      /customers          data=${payload}      headers=${headers}

    [Return]            ${resp} 

##PUT / CUSTOMERS##
Put Customer
    [Arguments]         ${payload}          ${customer_id}
    
    Create Session      zp-api              ${base_api_url}

    ${token}=           Get Session Token
    ${headers}          Create Dictionary   content-type=application/json                 authorization=${token}
    ${resp}=            Put Request         zp-api      /customers/${customer_id}          data=${payload}      headers=${headers}

    [Return]            ${resp} 

##GET / CUSTOMERS##
Get Customers
    Create Session      zp-api                  ${base_api_url}

    ${token}=           Get Session Token
    &{headers}=         Create Dictionary       content-type=application/json       authorization=${token}
    ${resp}=            Get Request         zp-api                  /customers      headers=${headers}

    [Return]            ${resp}

Get Unique Customer
    [Arguments]         ${customer_id}
    Create Session      zp-api                  ${base_api_url}

    ${token}=           Get Session Token
    &{headers}=         Create Dictionary            content-type=application/json       authorization=${token}
    ${resp}=            Get Request         zp-api   /customers/${customer_id}           headers=${headers}

    [Return]            ${resp}

##DELETE / CUSTOMERS##
Delete Customer
    [Arguments]         ${cpf}
    
    ${token}=           Get Session Token
    ${headers}          Create Dictionary   content-type=application/json   authorization=${token}
    ${resp}=            Delete Request      zp-api                          /customers/${cpf}    headers=${headers}

    [Return]            ${resp}    

##POST / EQUIPOS##
Post Equipo
    [Arguments]         ${payload}
    
    Create Session      zp-api              ${base_api_url}

    ${token}=           Get Session Token
    ${headers}          Create Dictionary   content-type=application/json   authorization=${token}
    ${resp}=            Post Request        zp-api      /equipos            data=${payload}      headers=${headers}

    [Return]            ${resp}