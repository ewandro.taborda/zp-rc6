****Settings***
Documentation       Recursos do Projeto

Library     SeleniumLibrary

Library     ../resources/libs/db.py

Resource    ../resources/kws.robot
Resource    ../resources/hooks.robot
Resource    ../resources/services.robot

Resource    ../resources/pages/LoginPage.robot
Resource    ../resources/pages/CustomersPage.robot
Resource    ../resources/pages/EquiposPage.robot
Resource    ../resources/pages/ContractsPage.robot

Resource    ../resources/components/Sidebar.robot
Resource    ../resources/components/Toaster.robot

***Variables***
${base_url}     http://zepalheta-web:3000/
${admin_user}   admin@zepalheta.com.br
${admin_pass}   pwd123
