import psycopg2

def execute_q(query):
    conn = psycopg2.connect(
        host='zepalheta-postgres',
        database='zepalheta',
        user='postgres',
        password='qaninja'
    )

    cur = conn.cursor()

    cur.execute(query)

    conn.commit()

    cur.close()
    conn.close()

def format_cpf(cpf):
    return cpf[:3] + "." + cpf[3:6] + "." + cpf[6:9] + "-" + cpf[9:]

def insert_customer(name, cpf, address, phone):

    cpf_formatado = format_cpf(cpf)

    query = "INSERT INTO public.customers(name,cpf,address,phone_number) "\
	    "VALUES ('{}', '{}', '{}', '{}');".format(name, cpf_formatado, address, phone)
    print(query)

    execute_q(query)

def remove_customer_by_cpf(cpf):

    cpf_formatado = format_cpf(cpf)

    query = "DELETE FROM public.customers WHERE cpf = '{}';".format(cpf_formatado)
    print(query)

    execute_q(query)   

def remove_equipo_by_name(name):

    query = "DELETE FROM public.equipos WHERE name = '{}';".format(name)
    print(query)

    execute_q(query)   

def insert_equipo(name, daily_price):
    
    query = "INSERT INTO public.equipos (name, daily_price)"\
	    "VALUES ('{}','{}')".format(name, daily_price)
    print(query)

    execute_q(query)