****Settings***
Documentation   Cadastro de Clientes - Tentativa

Resource        ../../../resources/base.robot

Suite Setup      Login Session  
Test Teardown    Finish Test Case
Suite Teardown   Finish Session  

Test Template   Tentativa de Cadastro de Clientes

***Keywords****
Tentativa de Cadastro de Clientes
    [Arguments]     ${nome}     ${cpf}      ${endereco}     ${telefone}     ${mensagem}
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    #      Nome do Cliente     CPF             Endereço                Telefone             
    ...    ${nome}             ${cpf}          ${endereco}             ${telefone}     
    Quando faço a inclusão desse cliente
    Então devo ver a mensagem de validação do campo:   ${mensagem}

***Test Cases***
#                       Nome do Cliente     CPF             Endereço             Telefone        Mensagem         
Nome em branco          ${EMPTY}            61354126980     Rua dos Bugs, 500    41993658877     Nome é obrigatório
CPF em branco           João da Silva       ${EMPTY}        Rua dos Bugs, 500    41993658877     CPF é obrigatório
Endereço em branco      João da Silva       61354126980     ${EMPTY}             41993658877     Endereço é obrigatório
Telefone em branco      João da Silva       61354126980     Rua dos Bugs, 500    ${EMPTY}        Telefone é obrigatório
CPF incorreto           João da Silva       999             Rua dos Bugs, 500    41993658877     CPF inválido
Telefone incorreto      João da Silva       61354126980     Rua dos Bugs, 500    41322           Telefone inválido

