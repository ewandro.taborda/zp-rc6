****Settings***
Documentation   Cadastro de Clientes

Resource        ../../../resources/base.robot

Suite Setup      Login Session   # Executa uma ou mais keywords antes da execução de todos os steps de cada caso de teste
Test Teardown    Finish Test Case
Suite Teardown   Finish Session  # Executa uma ou mais keywords após a execução de todos os steps de cada caso de teste

***Test Cases***
Novo Cliente
    [Tags]      smoke
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    #      Nome do Cliente     CPF             Endereço                Telefone             
    ...    Nome do Cliente     78392213513     Rua dos Testes, 5000    41999885577     
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:   Cliente cadastrado com sucesso!
    E esse cliente deve aparecer na listagem

Cliente já existente
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    #      Nome do Cliente     CPF             Endereço                Telefone             
    ...    Nome do Cliente     78392213513     Rua dos Testes, 5000    41999885577 
    Mas esse cliente já existe
    Quando faço a inclusão desse cliente
    Então devo ver uma notificação com o erro   Este CPF já existe no sistema!

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    #      Nome do Cliente     CPF             Endereço      Telefone             
    ...    ${EMPTY}            ${EMPTY}        ${EMPTY}      ${EMPTY}     
    Quando faço a inclusão desse cliente
    Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatório


