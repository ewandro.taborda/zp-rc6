****Settings***
Documentation   Exclusão de Clientes

Resource        ../../../resources/base.robot

Suite Setup      Login Session  
Test Teardown    Finish Test Case 
Suite Teardown   Finish Session  

***Test Cases***
Remover Cliente
    Dado que eu tenho um cliente indesejado:
    ...     Eduardo Henrique Silva      86248400822     Rua Ramal do Bueirinho, 5665        41988526441
    E acesso a listagem de clientes
    Quando eu removo esse cliente
    Então devo ver a notificação:  Cliente removido com sucesso!
    E esse cliente não deve aparecer na listagem