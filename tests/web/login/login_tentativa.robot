****Settings***
Documentation   Tentativa de Login

Resource        ../../../resources/base.robot

Suite Setup      Start Session      # Executa uma ou mais keywords somente uma vez antes de todos os casos de testes
Test Teardown    Finish Test Case   # Executa uma ou mais keywords após a execução de todos os steps de cada caso de teste
Suite Teardown   Finish Session     # Executa uma ou mais keywords uma única vez após finalizar todos os casos de testes

Test Template   Tentativa de Login

***Keywords***
Tentativa de Login
    [Arguments]     ${input_email}  ${input_senha}  ${output_mensagem}
    
    Dado que acesso a página Login
    Quando submeto minhas credenciais  ${input_email}  ${input_senha}  
    Então devo ver uma mensagem de alerta  ${output_mensagem}  

***Test Cases***
Cenário: Senha incorreta            admin@zepalheta.com.br      abc123      Ocorreu um erro ao fazer login, cheque as credenciais.
Cenário: Senha em branco            admin@zepalheta.com.br      ${EMPTY}    O campo senha é obrigatório!
Cenário: Email em branco            ${EMPTY}                    abc123      O campo email é obrigatório!
Cenário: Email e senha em branco    ${EMPTY}                    ${EMPTY}    Os campos email e senha não foram preenchidos!
Cenário: Email inválido             admin&zepalheta.com.br      abc123      Ocorreu um erro ao fazer login, cheque as credenciais.