****Settings***
Documentation   Login com Sucesso

Resource        ../../../resources/base.robot

Suite Setup      Start Session 
Test Teardown    Finish Test Case  
Suite Teardown   Finish Session  

***Test Cases***
Cenário: Login do Administrador
    [Tags]  smoke
    Dado que acesso a página Login
    Quando submeto minhas credenciais
#        Usuário                    Senha
    ...  admin@zepalheta.com.br     pwd123
    Então devo ser autenticado

#Cenário: Senha incorreta
#   [Tags]  inv_login
#   Dado que acesso a página Login
#   Quando submeto minhas credenciais  admin@zepalheta.com.br  abc123
#   Então devo ver uma mensagem de alerta  Ocorreu um erro ao fazer login, cheque as credenciais.
#
#Cenário: Senha em branco
#   [Tags]  inv_login
#   Dado que acesso a página Login
#   Quando submeto minhas credenciais  admin@zepalheta.com.br  ${EMPTY}
#   Então devo ver uma mensagem de alerta  O campo senha é obrigatório!
#
#Cenário: Email em branco
#   [Tags]  inv_login
#   Dado que acesso a página Login
#   Quando submeto minhas credenciais  ${EMPTY}  abc123
#   Então devo ver uma mensagem de alerta  O campo email é obrigatório!
#
#Cenário: Email e senha em branco
#   [Tags]  inv_login
#   Dado que acesso a página Login
#   Quando submeto minhas credenciais  ${EMPTY}  ${EMPTY}
#   Então devo ver uma mensagem de alerta  Os campos email e senha não foram preenchidos!