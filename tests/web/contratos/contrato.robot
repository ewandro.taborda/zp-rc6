****Settings***
Documentation   Cadastro de Contratos de Locação

Resource        ../../../resources/base.robot

Suite Setup      Login Session  
Test Teardown    Finish Test Case 
Suite Teardown   Finish Session  

***Test Cases***
Novo Contrato de Locação 
    Dado que eu tenho o seguinte cliente cadastrado         new_contract.json
    E este cliente deseja alugar o seguinte equipamento     new_equipo.json
    E acesso o formulário de cadastro de contratos
    Quando faço um novo contrato de locação
    Então devo ver a notificação:      Contrato criado com sucesso!