****Settings***
Documentation   Cadastro de Equipamentos

Resource        ../../../resources/base.robot

Suite Setup      Login Session  
Test Teardown    Finish Test Case 
Suite Teardown   Finish Session  

***Test Cases***
Novo Equipamento
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    #      Nome           Diária 
    ...    Guitarra       100,00     
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação:   Equipo cadastrado com sucesso!

Nome Obrigatório
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    #      Nome           Diária 
    ...    ${EMPTY}       50,00     
    Quando faço a inclusão desse equipamento
    Então devo ver a mensagem de validação do campo:    Nome do equipo é obrigatório

Valor Obrigatório
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    #      Nome           Diária 
    ...    Flauta         ${EMPTY}     
    Quando faço a inclusão desse equipamento
    Então devo ver a mensagem de validação do campo:    Diária do equipo é obrigatória

Equipamento Duplicado
    Dado que acesso o formulário de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    #      Nome           Diária 
    ...    Violão         100.50     
    Mas esse equipamento já está cadastrado
    Quando faço a inclusão desse equipamento
    Então devo ver uma mensagem de alerta   Ocorreu um error na criação de um equipo, tente novamente mais tarde!
 

