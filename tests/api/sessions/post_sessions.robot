*** Settings ***
Documentation       Login do Usuário - Sessions

Resource            ../../../resources/services.robot

*** Test Cases ***
Login com Sucesso
    ${resp}=          Post Session      admin@zepalheta.com.br      pwd123
    Status Should Be  200               ${resp}
    Should Be Equal As Strings          ${resp.reason}              OK
    Should Not Be Empty                 ${resp.json()["token"]}
    Length Should Be                    ${resp.json()["token"]}     188

Senha Incorreta
    ${resp}=          Post Session      admin@zepalheta.com.br              abc123
    Status Should Be  401               ${resp}
    Should Be Equal As Strings          ${resp.reason}        Unauthorized
    Dictionary Should Contain Item      ${resp.json()}        status        error               
    Dictionary Should Contain Item      ${resp.json()}        message       Incorrect email/password combination.
    
Usuário Inexistente
    ${resp}=          Post Session      401@zepalheta.com.br  pwd123
    Status Should Be  401               ${resp}
    Should Be Equal As Strings          ${resp.reason}        Unauthorized
    Dictionary Should Contain Item      ${resp.json()}        status        error               
    Dictionary Should Contain Item      ${resp.json()}        message       Incorrect email/password combination.

Usuário não Informado
    ${resp}=          Post Session      ${EMPTY}              pwd123
    Status Should Be  400               ${resp}
    Should Be Equal As Strings          ${resp.reason}        Bad Request
    Dictionary Should Contain Item      ${resp.json()}        error         Bad Request               
    Dictionary Should Contain Item      ${resp.json()}        message       "email" is not allowed to be empty

Senha não Informada
    ${resp}=          Post Session      admin@zepalheta.com.br              ${EMPTY}
    Status Should Be  400               ${resp}
    Should Be Equal As Strings          ${resp.reason}        Bad Request
    Dictionary Should Contain Item      ${resp.json()}        error         Bad Request               
    Dictionary Should Contain Item      ${resp.json()}        message       "password" is not allowed to be empty

    