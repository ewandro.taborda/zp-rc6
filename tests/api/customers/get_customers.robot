*** Settings ***
Documentation       Cadastro de Clientes - Customers

Resource            ../../../resources/services.robot

***Variables***
${invalid_id}       99999999-9999-9999-9999-999999999999

*** Test Cases ***
Get Customers List    
    ${list}=            Get Json File       /customers/list.json

    FOR     ${item}     IN      @{list['data']}
        Post Customer  ${item}
    END                   

    ${resp}=            Get Customers
    
    Status Should Be    200                 ${resp}
    Should Be Equal As Strings              ${resp.reason}              OK
    ${total}=           Get Length          ${resp.json()}
    Should Be True      ${total} > 0 

Get Unique Customer
    ${payload}=         Get Json File        /customers/unique.json
    
    Delete Customer     ${payload['cpf']}
    ${resp}=            Post Customer           ${payload}
    
    ${id_customer}=     Convert to String       ${resp.json()['id']}
    ${resp}=            Get Unique Customer     ${id_customer}

    Status Should Be    200                     ${resp}
    Should Be Equal As Strings                  ${resp.reason}          OK
    Should Be Equal As Strings                  ${resp.json()['cpf']}   ${payload['cpf']}

Customer Not Found
    ${resp}=            Get Unique Customer     ${invalid_id}

    Status Should Be    404                     ${resp}
    Should Be Equal As Strings                  ${resp.reason}        Not Found
    Dictionary Should Contain Item              ${resp.json()}        status        error               
    Dictionary Should Contain Item              ${resp.json()}        message       Customer not found