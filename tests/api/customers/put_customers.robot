*** Settings ***
Documentation       Cadastro de Clientes - Customers

Resource            ../../../resources/services.robot

***Variables***
${new_name}=        Ricardo Márcio Eduardo Ramos 

*** Test Cases ***
Update Customer
    ${payload}=         Get Json File               /customers/put.json
    
    Delete Customer     ${payload['cpf']}     
    ${resp}=            Post Customer               ${payload}

    ${custumer_id}=     Convert To String           ${resp.json()['id']}
    Set To Dictionary   ${payload}                  name                  ${new_name}         

    ${resp}=            Put Customer                ${payload}            ${custumer_id}        
    
    Status Should Be    204                         ${resp}
    Should Be Equal As Strings                      ${resp.reason}        No Content

    ${resp}=            Get Unique Customer         ${custumer_id} 
    
    Should Be Equal     ${resp.json()['name']}      ${new_name}      

