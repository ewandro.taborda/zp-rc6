*** Settings ***
Documentation       Cadastro de Clientes - Customers

Resource            ../../../resources/services.robot

*** Test Cases ***
New Customer
        
    &{payload}=         Get Json File       customers/new_customer.json
           
    Delete Customer     ${payload['cpf']}     
    
    ${resp} =           Post Customer       ${payload}

    Status Should Be    200                 ${resp}
    Should Be Equal As Strings              ${resp.reason}              OK 

Name is Required
    
    &{payload}=         Get Json File       customers/name_required.json
    ${resp}=            Post Customer       ${payload}
    
    Status Should Be    400                 ${resp}
    Should Be Equal As Strings              ${resp.reason}        Bad Request 
    Dictionary Should Contain Item          ${resp.json()}        message       "name" is required

CPF is Required
    
    &{payload}=         Get Json File       customers/cpf_required.json
    ${resp}=            Post Customer       ${payload}
    
    Status Should Be    400                 ${resp}
    Should Be Equal As Strings              ${resp.reason}        Bad Request 
    Dictionary Should Contain Item          ${resp.json()}        message       "cpf" is required

Address is Required
        
    &{payload}=         Get Json File       customers/address_required.json
    ${resp}=            Post Customer       ${payload}
    
    Status Should Be    400                 ${resp}
    Should Be Equal As Strings              ${resp.reason}        Bad Request 
    Dictionary Should Contain Item          ${resp.json()}        message       "address" is required

Phone Number is Required
       
    &{payload}=         Get Json File       customers/phone_required.json
    ${resp}=            Post Customer       ${payload}
    
    Status Should Be    400                 ${resp}
    Should Be Equal As Strings              ${resp.reason}        Bad Request 
    Dictionary Should Contain Item          ${resp.json()}        message       "phone_number" is required