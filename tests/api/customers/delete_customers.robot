*** Settings ***
Documentation       Cadastro de Clientes - Customers

Resource            ../../../resources/services.robot

***Variables***
${invalid_cpf}      999.999.999-99

*** Test Cases ***
Delete Customer
    ${payload}=         Get Json File        /customers/delete.json
    
    Delete Customer     ${payload['cpf']}
    ${resp}=            Post Customer           ${payload}
    
    ${resp}=            Delete Customer         ${payload['cpf']}

    Status Should Be    204                     ${resp}
    Should Be Equal As Strings                  ${resp.reason}          No Content
    
Customer Not Found
    ${resp}=            Delete Customer         ${invalid_cpf}

    Status Should Be    404                     ${resp}
    Should Be Equal As Strings                  ${resp.reason}        Not Found
    Dictionary Should Contain Item              ${resp.json()}        status        error               
    Dictionary Should Contain Item              ${resp.json()}        message       Customer not found